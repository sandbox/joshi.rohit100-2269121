<?php

/*
 * @file
 * File contains menu callbacks and form handlers.
 */

/**
 * Settings form handler for drupal_field_changer.  
 */
function _drupal_field_changer_settings($form, $form_state) {
    $content_type_list = node_type_get_types();
    foreach ($content_type_list as $content_type) {
        $node_type[$content_type->type] = $content_type->name;
    }
    $form['content-type-list'] = array(
        '#type' => 'select',
        '#title' => t('Select node/content type'),
        '#required' => TRUE,
        '#options' => $node_type,
        '#ajax' => array(
            'callback' => '_drupal_field_changer_field_ajax',
            'wrapper' => 'drupal-field-changer-field',
        ),
    );
    
    if (isset($form_state['values'])) {
        $fields = _drupal_field_changer_get_fields_info('node', $form_state['values']['content-type-list']);
    }
    $form['content-type-field'] = array(
            '#type' => 'select',
            '#required' => TRUE,
            '#title' => t('Select field that you want to change'),
            '#prefix' => '<div id="drupal-field-changer-field">',
            '#suffix' => '</div>',
            '#options' => !empty($fields) ? $fields : array(),
    );
    $form['new-type-field'] = array(
            '#type' => 'select',
            '#required' => TRUE,
            '#title' => t('Select new field type'),
            '#prefix' => '<div id="drupal-field-changer-new-field">',
            '#suffix' => '</div>',
            '#options' => array(),
    );
    $form['new-field-fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('New field\'s information'),
        '#collapsible' => TRUE,
    );
    $form['new-field-fieldset']['new-field-title'] = array(
            '#type' => 'textfield',
            '#required' => TRUE,
            '#description' => t('Specify the title that you want to display for the new field.'),
            '#title' => t('Title'),
    );
    $form['new-field-fieldset']['new-field-description'] = array(
            '#type' => 'textarea',
            '#description' => t('Specify the description that you want to display for the new field.'),
            '#title' => t('Description'),
    );
    $form['new-field-fieldset']['new-field-weight'] = array(
            '#type' => 'textfield',
            '#size' => 6,
            '#description' => t('Specify the weight of the new field.'),
            '#title' => t('Weight'),
            '#element_validate' => array('element_validate_integer_positive'), 
    );
    
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
    );
    return $form;
}

/**
 * Ajax handler for content type list field. 
 */
function _drupal_field_changer_field_ajax($form, $form_state) {
    return $form['content-type-field'];
}

function _drupal_field_changer_list() {
    return 'list';
}

/**
 * Callback method to get information about fields of a given bundle/content type.
 * @param type $entity_type
 * @param type $bundle
 */
function _drupal_field_changer_get_fields_info($entity_type = NULL, $bundle = NULL) {
    $list = array();
    $fields = _drupal_field_changer_get_fields($entity_type, $bundle);
    foreach ($fields as $field) {
        $field_info = field_info_instance($entity_type, $field, $bundle);
        $list[$field] = $field_info['label'];
    }
    return $list;
}

/**
 * Callback method to return fields for a given entity type in a given bundle.
 * @param type $entity_type Type of entity (Node / User / Comment).
 * @param type $bundle Bundle Name (Article / Page / Content type name for node).
 */
function _drupal_field_changer_get_fields($entity_type = NULL, $bundle = NULL) {
    $fields_info = array();
    $fields = field_info_field_map();
    foreach ($fields as $key => $val) {
        //If entity type is as specified and field is part of given bundle / content type.
        if (isset($val['bundles'][$entity_type]) && in_array($bundle, $val['bundles'][$entity_type])) {
            $fields_info[] = $key;
        }
    }
    return $fields_info;
}
